package org.bitbucket.nobby451;

import java.util.Calendar;
import java.util.Date;

public class DateSubtractor {

    public static int subtractDateOnly(Date date1, Date date2) {
        Calendar oldCalendar = Calendar.getInstance();
        Calendar newCalendar = Calendar.getInstance();
        if (date1.compareTo(date2) < 0) {
            oldCalendar.setTime(date1);
            newCalendar.setTime(date2);
        } else {
            oldCalendar.setTime(date2);
            newCalendar.setTime(date1);
        }
        int offset = 0;
        int oldDayOfYear = oldCalendar.get(Calendar.DAY_OF_YEAR);
        for (int i = oldCalendar.get(Calendar.YEAR), j = newCalendar.get(Calendar.YEAR); i < j; i++) {
            offset += oldCalendar.getActualMaximum(Calendar.DAY_OF_YEAR);
            oldCalendar.add(Calendar.YEAR, 1);
        }
        return newCalendar.get(Calendar.DAY_OF_YEAR) - oldDayOfYear + offset;
    }

}
