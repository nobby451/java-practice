package org.bitbucket.nobby451;

public class ExcelColumnIndexConverter {

    public static int letterToNumber(String letter) {
        int i = 0;
        for (char c : letter.toCharArray()) {
            i *= 26;
            i += c - 0x40;
        }
        return i;
    }

    public static String numberToLetter(int number) {
        StringBuilder builder = new StringBuilder();
        do {
            builder.insert(0, (char) (--number % 26 + 0x41));
        } while ((number /= 26) != 0);
        return builder.toString();
    }

}
