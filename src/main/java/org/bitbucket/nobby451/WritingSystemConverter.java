package org.bitbucket.nobby451;

public class WritingSystemConverter {

    /**
     * 全角カタカナを半角カタカナに変換する<br>
     * 変換できない文字は無視する
     * @param fullWidthKatakana 全角カタカナ
     * @return 半角カタカナ
     */
    public static String convertKatakanaFullToHalf(String fullWidthKatakana) {
        StringBuilder builder = new StringBuilder();
        for (char c : fullWidthKatakana.toCharArray()) {
            if (c < 'ァ') {
                continue;
            } else if (c < 'カ') {
                builder.append((char) ((c - 'ァ') / 2 + 'ｱ' - c % 2 * ('ｱ' - 'ｧ')));
            } else if (c < 'ッ') {
                builder.append((char) ((c - 'カ') / 2 + 'ｶ'));
                if ((c % 2) == 0) {
                    builder.append('ﾞ');
                }
            } else if (c == 'ッ') {
                builder.append('ｯ');
            } else if (c < 'ナ') {
                builder.append((char) ((c - 'ツ') / 2 + 'ﾂ'));
                if ((c % 2) == 1) {
                    builder.append('ﾞ');
                }
            } else if (c < 'ハ') {
                builder.append((char) (c - 'ナ' + 'ﾅ'));
            } else if (c < 'マ') {
                builder.append((char) ((c - 'ハ') / 3 + 'ﾊ'));
                if (c % 3 == 1) {
                    builder.append('ﾞ');
                } else if (c % 3 == 2) {
                    builder.append('ﾟ');
                }
            } else if (c < 'ャ') {
                builder.append((char) (c - 'マ' + 'ﾏ'));
            } else if (c < 'ラ') {
                builder.append((char) ((c - 'ャ') / 2 + 'ﾔ' - c % 2 * ('ﾔ' - 'ｬ')));
            } else if (c < 'ヮ') {
                builder.append((char) (c - 'ラ' + 'ﾗ'));
            } else if (c == 'ワ') {
                builder.append('ﾜ');
            } else if (c == 'ヲ') {
                builder.append('ｦ');
            } else if (c == 'ン') {
                builder.append('ﾝ');
            }
        }
        return builder.toString();
    }

}
