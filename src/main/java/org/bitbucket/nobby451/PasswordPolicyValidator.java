package org.bitbucket.nobby451;

public class PasswordPolicyValidator {

    public static boolean validateVariety(String password) {
        boolean number = false;
        boolean upper = false;
        boolean lower = false;
        for (char c : password.toCharArray()) {
            if (!number && 0x2F < c && c < 0x3A) {
                number = true;
            }
            if (!upper && 0x40 < c && c < 0x5B) {
                upper = true;
            }
            if (!lower && 0x60 < c && c < 0x7B) {
                lower = true;
            }
            if (number && upper && lower) {
                return true;
            }
        }
        return false;
    }

}
