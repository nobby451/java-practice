package org.bitbucket.nobby451;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.Test;

public class DateSubtractorTest {

    @Test
    public void testSubtractDateOnly() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Assert.assertEquals(29, DateSubtractor.subtractDateOnly(dateFormat.parse("2011-12-01T01:00:00"), dateFormat.parse("2011-12-30T23:00:00")));
        Assert.assertEquals(29, DateSubtractor.subtractDateOnly(dateFormat.parse("2011-12-01T23:00:00"), dateFormat.parse("2011-12-30T01:00:00")));
        Assert.assertEquals(120, DateSubtractor.subtractDateOnly(dateFormat.parse("2011-12-01T00:00:00"), dateFormat.parse("2012-03-30T00:00:00")));
        Assert.assertEquals(119, DateSubtractor.subtractDateOnly(dateFormat.parse("2012-12-01T00:00:00"), dateFormat.parse("2013-03-30T00:00:00")));
        Assert.assertEquals(849, DateSubtractor.subtractDateOnly(dateFormat.parse("2012-12-01T00:00:00"), dateFormat.parse("2015-03-30T00:00:00")));
    }

}
