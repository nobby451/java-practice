package org.bitbucket.nobby451;

import org.junit.Assert;
import org.junit.Test;

public class ExcelColumnIndexConverterTest {

    @Test
    public void testLetterToNumber() {
        Assert.assertEquals(1, ExcelColumnIndexConverter.letterToNumber("A"));
        Assert.assertEquals(26, ExcelColumnIndexConverter.letterToNumber("Z"));
        Assert.assertEquals(27, ExcelColumnIndexConverter.letterToNumber("AA"));
        Assert.assertEquals(702, ExcelColumnIndexConverter.letterToNumber("ZZ"));
        Assert.assertEquals(703, ExcelColumnIndexConverter.letterToNumber("AAA"));
    }

    @Test
    public void testNumberToLetter() {
        Assert.assertEquals("A", ExcelColumnIndexConverter.numberToLetter(1));
        Assert.assertEquals("Z", ExcelColumnIndexConverter.numberToLetter(26));
        Assert.assertEquals("AA", ExcelColumnIndexConverter.numberToLetter(27));
        Assert.assertEquals("ZZ", ExcelColumnIndexConverter.numberToLetter(702));
        Assert.assertEquals("AAA", ExcelColumnIndexConverter.numberToLetter(703));
    }

}
