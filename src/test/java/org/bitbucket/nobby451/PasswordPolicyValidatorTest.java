package org.bitbucket.nobby451;

import org.junit.Assert;
import org.junit.Test;

public class PasswordPolicyValidatorTest {

    @Test
    public void testValidateVariety() {
        Assert.assertTrue(PasswordPolicyValidator.validateVariety("0Aa"));
        Assert.assertFalse(PasswordPolicyValidator.validateVariety("/Aa"));
        Assert.assertFalse(PasswordPolicyValidator.validateVariety("0@a"));
        Assert.assertFalse(PasswordPolicyValidator.validateVariety("0A`"));
        Assert.assertTrue(PasswordPolicyValidator.validateVariety("9Zz"));
        Assert.assertFalse(PasswordPolicyValidator.validateVariety(":Zz"));
        Assert.assertFalse(PasswordPolicyValidator.validateVariety("9[z"));
        Assert.assertFalse(PasswordPolicyValidator.validateVariety("9Z{"));
    }

}
